package jdbcsample.controller;

import jdbcsample.dao.CarDAO;
import jdbcsample.model.Car;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Денис on 25.11.2015.
 */
public class CarController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String PAGE_EDIT = "/addCar.jsp";
    private static final String PAGE_LIST = "/shop.jsp";
    private static final String ACTION_VALUE = "action";
    private static final String ACTION_DELETE = "delete";
    private static final String ACTION_EDIT = "edit";
    private static final String ACTION_LIST = "list";
    private static final String ATTRIBUTE_ITEM = "item";
    private static final String ATTRIBUTE_LIST = "list";
    private static final String PARAMETER_ENTITY_ID = "id";

    private CarDAO dao;

    public CarController() {
        super();
        dao = CarDAO.getInstance();
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter(ACTION_VALUE);
        if (action != null)
            switch (action) {
                case ACTION_DELETE:
                    int itemId = Integer.parseInt(request
                            .getParameter(PARAMETER_ENTITY_ID));
                    dao.delete(itemId);
                    forward = PAGE_LIST;
                    request.setAttribute(ATTRIBUTE_LIST, dao.selectAll());
                    break;

                case ACTION_EDIT:
                    forward = PAGE_EDIT;
                    int userId = Integer.parseInt(request
                            .getParameter(PARAMETER_ENTITY_ID));
                    Car item = dao.select(userId);
                    request.setAttribute(ATTRIBUTE_ITEM, item);
                    break;

                case ACTION_LIST:
                    forward = PAGE_LIST;
                    request.setAttribute(ATTRIBUTE_LIST, dao.selectAll());
                    break;

                default:
                    forward = PAGE_EDIT;
                    break;
            }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        Car item = new Car();
        item.setModel(request.getParameter("model"));
        try {
            item.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date_of_create")));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("trouble with date");
        }
        item.setColor(request.getParameter("color"));

        String userid = request.getParameter(PARAMETER_ENTITY_ID);
        if (userid == null || userid.isEmpty()) {
            dao.insert(item);
        } else {
            item.setId(Long.parseLong(userid));
            dao.update(item);
        }
        RequestDispatcher view = request.getRequestDispatcher(PAGE_LIST);
        request.setAttribute(ATTRIBUTE_LIST, dao.selectAll());
        view.forward(request, response);
    }
}
