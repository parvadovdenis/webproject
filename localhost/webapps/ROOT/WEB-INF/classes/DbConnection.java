package jdbcsample.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by Денис on 18.11.2015.
 */
public class DbConnection {


    private static String USER = "root";
    private static String PASSWORD = "";
    private static String URL = "jdbc:mysql://localhost/carshop";

    private static Connection INSTANCE;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Properties prop = new Properties();
        FileInputStream in = null;
        try {
            in = new FileInputStream("database.properties");

            prop.load(in);
            in.close();
        } catch (IOException e) {
            System.out.println("Error");
        }
        URL = prop.getProperty("jdbc.url");
        USER = prop.getProperty("jdbc.username");
        PASSWORD = prop.getProperty("jdbc.password");
    }

    private DbConnection() {

    }

    public static Connection getInstance() {
        try {
            if (INSTANCE == null || INSTANCE.isClosed()) {

                INSTANCE = DriverManager.getConnection(URL, USER, PASSWORD);
            }
        } catch (Exception e) {
// TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("No connection");

        }
        return INSTANCE;
    }


}