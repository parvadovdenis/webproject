﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
    <title>
        Car shop
    </title>
    <style>
        body {
            background: url(src/back.jpg);
        }

        p {
            font-style: italic;
            font-size: 150%;
        }

        tr {
            text-align: center;
            font-style: italic;
            font-size: 150%;
            height: 50px;
        }

        .addCar {
            position: fixed;
            bottom: 100px;
            right: 190px;
            width: 100px;
            height: 35px;
            z-index: 999;
        }

        .addCarText {
            position: fixed;
            bottom: 50px;
            right: 189px;
            width: 100px;
            height: 35px;
            z-index: 999;
        }
    </style>
</head>

<body>

<table align="center" border="1" width="30%">
    <tr>
        <td> №</td>
        <td> Model</td>
        <td> Color</td>
        <td> Date</td>
    </tr>
    <tbody>
    <c:forEach items="${list}" var="item">
        <tr>
            <td><c:out value="${item.id}"/></td>
            <td><c:out value="${item.model}"/></td>
            <td><c:out value="${item.color}"/></td>
            <td><c:out value="${item.date_of_create}"/></td>
            <td><a
                    href="CarController?action=edit&id=<c:out value="${item.id}"/>">Update</a></td>
            <td><a
                    href="CarController?action=delete&id=<c:out value="${item.id}"/>">Delete</a></td>
        </tr>
    </c:forEach>
    <%--</tbody>--%>
</table>

<div class="addCar">
    <a class="addCar" href="CarController?action=insert" title="Добавить машину в таблицу">
        <img src="src/car.png" width="80"/>
    </a>

</div>
<div class="addCarText">
    <p>Add car</p>
</div>
</body>
</html>