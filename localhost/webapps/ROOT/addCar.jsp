﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
    <title>
        Add car
    </title>
    <style>
        body {
            background: url(src/back.jpg);
        }

        p {
            font-style: italic;
            font-size: 150%;
        }

        .addCar {
            position: fixed;
            bottom: 100px;
            right: 200px;
            width: 100px;
            height: 35px;
            z-index: 999;
        }

        .addCarText {
            position: fixed;
            bottom: 50px;
            right: 200px;
            width: 100px;
            height: 35px;
            z-index: 999;
        }
    </style>
</head>
<body>
<form align="center">
    <p><b>MODEL</b><br>
        <input type="text" size="40">
    </p>

    <p><b>COLOR</b><br>
        <input type="text" size="40">
    </p>

    <p><b>DATE</b><br>
        <input type="text" size="40">
    </p>

    <div class="addCar">
        <a class="addCar" href="shop.jsp" title="Добавить машину в таблицу">
            <img src="src/car.png" width="80"/>
        </a>

    </div>
    <div class="addCarText">
        <p>Add car</p>
    </div>

</form>

<form method="POST" action='CarController' name="form">
    id : <input type="text" readonly="readonly" name="id"
                value="<c:out value="${item.id}" />"/> <br/>
    model : <input
        type="text" name="model" value="<c:out value="${item.model}" />"/><br/>
    color : <input type="text" name="color"
                   value="<c:out value="${item.color}" />"/> <br/>
    date : <input type="text" name="date"
                  value="<c:out  value="${item.date}" />"/>
    <br/> <input
        type="submit" value="Submit"/>
</form>
</body>
</html>