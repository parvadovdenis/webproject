﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
    <title>
        Add car
    </title>
    <style>
        body {
            background: url(src/back.jpg);
        }

        p {
            font-style: italic;
            font-size: 150%;
        }

        .addCar {
            position: fixed;
            bottom: 100px;
            right: 200px;
            width: 100px;
            height: 35px;
            z-index: 999;
        }

        .addCarText {
            position: fixed;
            bottom: 50px;
            right: 189px;
            width: 100px;
            height: 35px;
            z-index: 999;
        }
    </style>
</head>
<body>

<form align="center" method="POST" action='CarController' name="form">
    <p><b>ID</b><br></p>
    <input type="text" readonly="readonly" name="id" size="40"
           value="<c:out value="${item.id}" />"/> <br/>

    <p><b>MODEL</b><br></p>
    <input type="text" name="model" size="40" value="<c:out value="${item.model}" />"/>


    <p><b>COLOR</b><br></p>
    <input type="text" name="color" size="40" value="<c:out value="${item.color}" />"/>

    <p><b>DATE</b><br></p>
    <input type="text" name="date" size="40"
           value="<fmt:formatDate pattern="MM/dd/yyyy" value="${item.date}" />"/><br/>

    <div class=" addCar">
        <input type="image" src="src/carSubmit.png"/>
    </div>

</form>


<div class="addCarText">
    <p>Add car</p>
</div>

</body>
</html>