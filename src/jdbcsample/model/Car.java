package jdbcsample.model;

import java.util.Date;

/**
 * Created by Денис on 31.10.2015.
 */
public class Car implements Comparable<Car> {
    private String model;
    private String color;
    private Date date;
    private Long id;


    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Car() {
        super();
    }

    public Car(String model, Date date, String color) {

        super();
        this.model = model;
        this.color = color;
        this.date = date;

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (color != null ? !color.equals(car.color) : car.color != null) return false;
        if (date != null ? !date.equals(car.date) : car.date != null) return false;
        if (model != null ? !model.equals(car.model) : car.model != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = model != null ? model.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", date=" + date +
                "}\n";
    }

    @Override
    public int compareTo(Car o) {
        return this.model.compareTo(o.getModel());
    }


}