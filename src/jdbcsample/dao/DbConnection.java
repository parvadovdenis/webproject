package jdbcsample.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Денис on 18.11.2015.
 */
public class DbConnection {


    private static String USER = "root";
    private static String PASSWORD = "";
    private static String URL = "jdbc:mysql://localhost/carshop";

    private static Connection INSTANCE;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private DbConnection() {

    }

    public static Connection getInstance() {
        try {
            if (INSTANCE == null || INSTANCE.isClosed()) {

                INSTANCE = DriverManager.getConnection(URL, USER, PASSWORD);
            }
        } catch (Exception e) {
// TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("No connection");

        }
        return INSTANCE;
    }


}