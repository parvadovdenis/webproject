package jdbcsample.dao;

import jdbcsample.model.Car;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarDAO {
    private static String SQL_UPDATE = "UPDATE carshop SET model=?, date_of_create=?, color = ? WHERE id = ?";
    private static String SQL_INSERT = "INSERT INTO carshop (model, date_of_create, color) VALUE (?, ?, ?); ";
    private static String SQL_SELECT = "SELECT * FROM carshop;";
    private static String SQL_DELETE = "DELETE  FROM carshop;";
    private static String SQL_SELECT_BY_ID = "SELECT * FROM carshop WHERE id = ?;";
    private static String SQL_DELETE_BY_ID = "DELETE FROM carshop WHERE id = ?;";
    private static final String FIELD_ID = "id";
    private static final String FIELD_MODEL = "model";
    private static final String FIELD_COLOR = "color";
    private static final String FIELD_DATE_OF_CREATE = "date_of_create";
    private static CarDAO CAR_DAO = null;

    private CarDAO() {

    }

    public static CarDAO getInstance() {
        if (CAR_DAO == null) {
            CAR_DAO = new CarDAO();
        }
        return CAR_DAO;
    }

    public boolean delete(Car item) {
        return delete(item.getId());
    }

    public boolean delete(long id) {
        Connection con = null;
        try {
            con = DbConnection.getInstance();
            PreparedStatement st = con.prepareStatement(SQL_DELETE_BY_ID);
            st.setLong(1, id);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
        }
    }

    public boolean deleteAll() {
        Connection con = null;
        try {
            con = DbConnection.getInstance();
            PreparedStatement st = con.prepareStatement(SQL_DELETE);
            int n = st.executeUpdate();
            if (n == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
        }
    }

    public List<Car> selectAll() {
        Connection con = null;
        List<Car> list = new ArrayList<Car>();
        try {
            con = DbConnection.getInstance();
            Statement st = con.createStatement();
            ResultSet result = st.executeQuery(SQL_SELECT);
            while (result.next()) {
                list.add(extractCar(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return list;
        } finally {
            close(con);
        }
        return list;
    }

    public Car select(int id) {
        Connection con = null;
        Car item = null;
        try {
            con = DbConnection.getInstance();
            PreparedStatement st = con.prepareStatement(SQL_SELECT_BY_ID);
            st.setInt(1, id);
            ResultSet result = st.executeQuery();
            if (result.next()) {
                item = extractCar(result);
            }
            return item;
        } catch (Exception e) {
            e.printStackTrace();
            return item;
        } finally {
            close(con);
        }
    }


    public boolean insert(Car item) {
        return insert(item.getModel(), item.getDate(), item.getColor());
    }

    public boolean insert(String model, Date date_of_create, String color) {
        Connection con = null;
        try {
            con = DbConnection.getInstance();
            PreparedStatement st = con.prepareStatement(SQL_INSERT);
            st.setString(1, model);
            st.setLong(2, date_of_create.getTime());
            st.setString(3, color);
            int n = st.executeUpdate();
            if (n == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
        }
    }

    public void update(Car item) {
        Connection con = null;
        try {
            con = DbConnection.getInstance();
            PreparedStatement preparedStatement = con
                    .prepareStatement(SQL_UPDATE);

            preparedStatement.setString(1, item.getModel());
            preparedStatement.setLong(2, item.getDate().getTime());
            preparedStatement.setString(3, item.getColor());
            preparedStatement.setLong(4, item.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private Car extractCar(ResultSet rs) throws SQLException {
        Car item = new Car();
        item.setId(rs.getLong(FIELD_ID));
        item.setModel(rs.getString(FIELD_MODEL));
        item.setDate(new Date(rs.getLong(FIELD_DATE_OF_CREATE)));
        item.setColor(rs.getString(FIELD_COLOR));
        return item;
    }
}
