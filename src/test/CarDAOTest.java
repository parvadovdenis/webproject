package test;

import jdbcsample.dao.CarDAO;
import jdbcsample.model.Car;
import junit.framework.TestCase;

import java.util.Date;
import java.util.List;


public class CarDAOTest extends TestCase {

    public void testDAO() {
        CarDAO.getInstance().deleteAll();
        assertEquals(0, CarDAO.getInstance().selectAll().size());

        Car b = new Car("abc", new Date(), "bcd");
        CarDAO.getInstance().insert(b);

        List<Car> cars = CarDAO.getInstance().selectAll();
        assertEquals(1, cars.size());
        assertEquals(b.getColor(), cars.get(0).getColor());
        assertEquals(b.getModel(), cars.get(0).getModel());
        assertEquals(b.getDate(), cars.get(0).getDate());
        CarDAO.getInstance().delete(cars.get(0));
        assertEquals(0, CarDAO.getInstance().selectAll().size());

        CarDAO.getInstance().insert(b);
        CarDAO.getInstance().deleteAll();
        assertEquals(0, CarDAO.getInstance().selectAll().size());
    }
}