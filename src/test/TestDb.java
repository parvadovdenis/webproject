package test;

import jdbcsample.dao.CarDAO;
import jdbcsample.model.Car;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Денис on 22.11.2015.
 */
public class TestDb {
    public static void main(String[] args) {
        CarDAO.getInstance().deleteAll();
        Car car = new Car("a", new Date(), "a");
        CarDAO.getInstance().insert(car);
        CarDAO.getInstance().update(car);
        System.out.println(CarDAO.getInstance().selectAll());

//        System.out.println(CarDAO.getInstance().selectAll());
//        List<Car> cars = CarDAO.getInstance().selectAll();
//        System.out.println("cars: " + cars);
//        CarDAO.getInstance().delete(cars.get(0));

    }


}
